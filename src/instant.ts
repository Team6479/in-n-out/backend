import { Moment } from './moments';
import { Timeline } from './timeline';

export interface Instant {
    entities: {
        [eid: string]: Entity
    };
}
export interface Entity {
    lvl: number;
    name: string;
    pubkey: string;
    privkey: string;
    sessions: Session[];
}
export interface Session {
    start: number;
    end: number;
    valid: boolean;
    location: string;
    approver: string;
}

interface EventHandler {
    handle(Moment, Instant): Instant;
}
interface ScratchPad {
    sessions: {
        [eid: string]: {
            start: number;
            location: string;
        }
    }
}
function handlerBuilder(moment: Moment, tmp: ScratchPad, instant: Instant, reqEntityExist: boolean, reqDataType: string,
        handler: (Moment, ScratchPad, Instant) => void): () => void {
    if ((instant.entities[moment.character] || !reqEntityExist) && (reqDataType && typeof moment.data == reqDataType || moment.data == null)) {
        return () => { handler(moment, tmp, instant); };
    } else {
        return () => null;
    }
}

const handlerList: { [name: string]: [(Moment, ScratchPad, Instant) => void, boolean, string] } = {
    'arbitrary': [() => true, false, null],
    'entity': [(moment, tmp, instant) => {
        if (!instant.entities[moment.character]) {
            instant.entities[moment.character] = { lvl: 0, name: null, pubkey: null, privkey:null, sessions: [] };
        }
    }, false, null],
    'rm-entity': [(moment: Moment, tmp: ScratchPad, instant: Instant) => {
        delete instant.entities[moment.character];
    }, true, null],
    'name': [(moment: Moment, tmp: ScratchPad, instant: Instant) => {
        instant.entities[moment.character].name = <string>moment.data;
    }, true, 'string'],
    'pubkey': [(moment: Moment, tmp: ScratchPad, instant: Instant) => {
        if (instant.entities[moment.actor].lvl >= instant.entities[moment.character].lvl) {
            instant.entities[moment.character].pubkey = <string>moment.data;
        }
    }, true, 'string'],
    'privkey': [(moment: Moment, tmp: ScratchPad, instant: Instant) => {
        if (instant.entities[moment.actor].lvl >= instant.entities[moment.character].lvl) {
            instant.entities[moment.character].privkey = <string>moment.data;
        }
    }, true, 'string'],
    'grant': [(moment: Moment, tmp: ScratchPad, instant: Instant) => {
        if (instant.entities[moment.actor].lvl >= moment.data
                && instant.entities[moment.actor].lvl >= instant.entities[moment.character].lvl
                && moment.data >= 0 && moment.data <= 3) {
            instant.entities[moment.character].lvl = <number>moment.data;
        }
    }, true, 'number'],
    'signin': [(moment: Moment, tmp: ScratchPad, instant: Instant) => {
        if (tmp.sessions[moment.character]) { // already signed in
            instant.entities[moment.character].sessions.push({ // cancel existing session
                start: tmp.sessions[moment.character].start,
                location: tmp.sessions[moment.character].location,
                end: null, valid: false, approver: null
            });
        }
        tmp.sessions[moment.character] = {
            start: moment.timestamp,
            location: <string>moment.data
        };
    }, true, 'string'],
    'signout': [(moment: Moment, tmp: ScratchPad, instant: Instant) => {
        if (tmp.sessions[moment.character]) { // signed in
            instant.entities[moment.character].sessions.push({ // cancel existing session
                start: tmp.sessions[moment.character].start,
                end: moment.timestamp,
                location: tmp.sessions[moment.character].location,
                valid: true,
                approver: <string>moment.data
            });
            delete tmp.sessions[moment.character];
        }
    }, true, 'string'],
    'cancel': [(moment: Moment, tmp: ScratchPad, instant: Instant) => {
        if (tmp.sessions[moment.character]) { // signed in
            instant.entities[moment.character].sessions.push({ // cancel existing session
                start: tmp.sessions[moment.character].start,
                end: moment.timestamp,
                location: tmp.sessions[moment.character].location,
                valid: false,
                approver: null
            });
            delete tmp.sessions[moment.character];
        }
    }, true, null],
    'conclude': [(moment: Moment, tmp: ScratchPad, instant: Instant) => {
        for (const eid in tmp.sessions) { // all existing sessions
            if (tmp.sessions[eid].location == <string>moment.data) { // location matches, cancel
                instant.entities[moment.character].sessions.push({ // cancel existing session
                    start: tmp.sessions[moment.character].start,
                    end: moment.timestamp,
                    location: tmp.sessions[moment.character].location,
                    valid: false,
                    approver: null
                });
                delete tmp.sessions[moment.character];
            }
        }
    }, false, null]
}

export class Now {
    now: Instant;
    private scratchpad: ScratchPad;
    private static instance: Now;

    private constructor() {
        this.now = {entities: {
            'root': { // necessary hack
                'lvl': 3,
                'name': null,
                'privkey': null,
                'pubkey': null,
                'sessions': []
            }
        }};
        this.scratchpad = {sessions:{}};
    }

    static getInstance(): Now {
        if (!Now.instance) {
            Now.clear().rebuild();
        }
        return Now.instance;
    }

    static clear(): Now {
        Now.instance = new Now();
        return Now.instance;
    }

    rebuild(): Now {
        Now.clear();
        Timeline.getInstance().getList().forEach((mid) => {
            this.compute(Timeline.getInstance().getMoment(mid));
        });
        return Now.getInstance();
    }

    private compute(moment: Moment): Now { // this WILL break things if the Moment isn't the latest
        if (handlerList[moment.event]) { // only run the function if the event is valid
            // TODO: decomplicate
            handlerBuilder(moment, this.scratchpad, Now.getInstance().now, handlerList[moment.event][1], handlerList[moment.event][2], handlerList[moment.event][0])();
        }
        return Now.getInstance();
    }
}