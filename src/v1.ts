import express from 'express';

import * as auth from './auth';
import { Timeline } from './timeline';
import { Moment, check_mid } from './moments';
import { Now } from './instant';

const app = express();

app.get('/version', (req, res) => {
    res.send('1.0.0');
});

app.get('/timeline', (req, res) => {
    if (auth.check(<string>req.query['inq'], 1)) {
        auth.encrypt(<string>req.query['inq'], Timeline.getInstance().getList().join('\n'))
            .then((encrypted: string) => { res.send(encrypted); })
            .catch(() => { res.status(500).send('error: could not encrypt requested data') });
    } else {
        res.status(403).send('error: invalid inquisitor');
    }
});

app.get('/moment/:mid', (req, res) => {
    if (auth.check(<string>req.query['inq'], 1)) {
        if (Timeline.getInstance().getMoment(<string>req.params['mid'])) {
            auth.encrypt(<string>req.query['inq'], JSON.stringify(Timeline.getInstance().getMoment(<string>req.params['mid'])))
                .then((encrypted: string) => { res.send(encrypted); })
                .catch(() => res.status(500).send('error: could not encrypt requested data'));
        } else {
            res.status(404).send('error: the requested moment could not be found');
        }
    } else {
        res.status(403).send('error: invalid inquisitor');
    }
});

app.post('/moment/:mid', (req, res) => {
    if (Timeline.getInstance().getMoment(<string>req.params['mid'])) {
        res.status(409).send('error: moments are immutable');
    } else {
        const body = [];
        req.on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            auth.verify(Buffer.concat(body).toString()).then((moment: Moment) => {
                if (moment != null) {
                    if (auth.check(moment.actor, 2)) {
                        if (check_mid(<string>req.params['mid'])) {
                            Timeline.getInstance().push(<string>req.params['mid'], moment);
                            Now.getInstance().rebuild();
                            res.send('success');
                        } else {
                            res.status(400).send('error: invalid mid');
                        }
                    } else {
                        res.status(403).send('error: invalid actor');
                    }
                } else {
                    res.status(403).send('error: invalid signature');
                }
            })
            .catch(() => { res.status(400).send('error: verification failed') });
        })
    }
});

app.get('/instant', (req, res) => {
    if (auth.check(<string>req.query['inq'], 1)) {
        auth.encrypt(<string>req.query['inq'], JSON.stringify(Now.getInstance().now))
            .then((encrypted: string) => { res.send(encrypted); })
            .catch(() => { res.status(500).send('error: could not encrypt requested data') });
    } else {
        res.status(403).send('error: invalid inquisitor');
    }
});

app.get('/lognow', (req, res) => {
    // TODO: dev purposed only, remove in final release
    console.log(Now.getInstance().now);
    res.status(200).send('logged');
});

app.get('/instant/entities/:eid', (req, res) => {
    if (auth.check(<string>req.query['inq'], 1)) {
        auth.encrypt(<string>req.query['inq'], JSON.stringify(Now.getInstance().now.entities[<string>req.params['eid']]))
            .then((encrypted: string) => { res.send(encrypted); })
            .catch(() => { res.status(500).send('error: could not encrypt requested data') });
    } else {
        res.status(403).send('error: invalid inquisitor');
    }
});

app.get('/instant/entities/:eid/name', (req, res) => {
    if (auth.check(<string>req.query['inq'], 1)) {
        auth.encrypt(<string>req.query['inq'], JSON.stringify(Now.getInstance().now.entities[<string>req.params['eid']].name))
            .then((encrypted: string) => { res.send(encrypted); })
            .catch(() => { res.status(500).send('error: could not encrypt requested data') });
    } else {
        res.status(403).send('error: invalid inquisitor');
    }
});

app.get('/instant/entities/:eid/key/pub', (req, res) => { // this endpoint is public
    if (Now.getInstance().now.entities[<string>req.params['eid']] && Now.getInstance().now.entities[<string>req.params['eid']].pubkey) {
        res.send(Now.getInstance().now.entities[<string>req.params['eid']].pubkey)
    } else {
        res.status(404).send('error: public key could not be found');
    }
});

app.get('/instant/entities/:eid/key/priv', (req, res) => { // this endpoint is public
    if (Now.getInstance().now.entities[<string>req.params['eid']] && Now.getInstance().now.entities[<string>req.params['eid']].privkey) {
        res.send(Now.getInstance().now.entities[<string>req.params['eid']].privkey)
    } else {
        res.status(404).send('error: private key could not be found');
    }
});

export {app as v1};