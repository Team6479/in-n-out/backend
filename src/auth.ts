import * as openpgp from 'openpgp';

import { Now } from './instant';
import { Moment } from './moments';

export function check(eid: string, minlvl: number): boolean {
    return eid && Now.getInstance().now.entities[eid] && Now.getInstance().now.entities[eid].lvl >= minlvl;
}

function extractMoment(data: string): Moment {
    // mild hack; probably stable but definitely inelegant
    return JSON.parse(data.match(/{.*}/)[0]);
}

export async function verify(data: string): Promise<Moment> {
    const moment: Moment = extractMoment(data);
    return openpgp.cleartext.readArmored(data).then((msg) => {
        return openpgp.key.readArmored(Now.getInstance().now.entities[moment.actor].pubkey)
        .then((key) => openpgp.verify({
            message: msg,
            publicKeys: key.keys
        })).then((verified) => (verified.signatures[0].verified) ? moment :  null);
    }).catch((e)=>{console.log(e);return null;});
}

export async function encrypt(inq: string, data: string): Promise<string> {
    return openpgp.key.readArmored(Now.getInstance().now.entities[inq].pubkey).then((keyres) =>
        openpgp.encrypt({
            message: openpgp.message.fromText(data),
            publicKeys: keyres.keys
        })
    ).then((enc) => enc.data);
}