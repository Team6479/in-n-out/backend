export interface Moment {
    v: string;
    event: string;
    timestamp: number;
    actor: string;
    character: string;
    data: unknown;
}

const mid_regex = new RegExp('^([0-9]|[A-F]){32}$');
export function check_mid(mid: string): boolean {
    return mid_regex.test(mid);
}