import fs from 'fs';

import { Moment } from './moments'

const dir = '.in-n-out';

export function writeMoment(mid: string, moment: Moment): boolean {
    if (fs.existsSync(dir + '/moments/' + mid)) {
        return false;
    } else {
        fs.writeFileSync(dir + '/moments/' + mid, JSON.stringify(moment));
        return true;
    }
}

export function readMoment(mid: string): Moment {
    return JSON.parse(fs.readFileSync(dir + '/moments/' + mid).toString());
}

export function listMoments(): string[] {
    const sortable = [];
    fs.readdirSync(dir + '/moments').forEach((mid: string) => {
        sortable.push([mid, this.readMoment(mid).timestamp]);
    });
    return sortable.sort((a, b) => a[1] - b[1]).map(x => x[0]);
}