import { Moment } from './moments';
import * as db from './db';

export class Timeline {
    timeline: {
        [mid: string]: Moment
    };
    private static instance: Timeline;

    private constructor() {
        this.timeline = {};
        db.listMoments().forEach((mid: string) => {
            this.timeline[mid] = db.readMoment(mid);
        });
    }

    static getInstance(): Timeline {
        if (!this.instance) {
            this.instance = new Timeline();
        }
        return this.instance;
    }

    getList(): string[] {
        const sortable = [];
        for (const mid in this.timeline) {
            sortable.push([mid, this.timeline[mid].timestamp]);
        }
        return sortable.sort((a, b) => a[1] - b[1]).map(x => x[0]);
    }

    getMoment(mid: string): Moment {
        return this.timeline[mid];
    }

    // WARNING: this does not automatically rebuild
    // generally, you should rebuild Now after running this
    push(mid: string, moment: Moment): boolean {
        if (this.getMoment(mid)) {
            return false;
        } else {
            db.writeMoment(mid, moment);
            this.timeline[mid] = moment;
            return true;
        }
    }

    getStoredMoment(mid: string): Moment {
        return db.readMoment(mid);
    }
}