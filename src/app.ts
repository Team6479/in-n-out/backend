import express from 'express';

import { v1 } from './v1';

const app = express();

app.use('/api/v1', v1);

const port = 4000;
app.listen(port, err => {
  if (err) {
    console.error(err);
  }
  console.log(`server is listening on ${port}`);
});